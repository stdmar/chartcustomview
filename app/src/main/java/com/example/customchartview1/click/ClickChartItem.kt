package com.example.customchartview1.click

import com.example.customchartview1.ChartItem

class ClickChartItem(
    val index: Int,
    val item: ChartItem,
    private val xPair: Pair<Float, Float>,
    private val yPair: Pair<Float, Float>
){
    fun filterClickPredicate(x: Float, it: ClickChartItem, y: Float) =
        (x >= it.xPair.first && x <= it.xPair.second) && (y >= it.yPair.first && y <= it.yPair.second)
}