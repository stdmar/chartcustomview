package com.example.customchartview1

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.widget.Toast
import com.example.customchartview1.click.ClickChartItem

class DrawHorizontalChart(
    private val paddingLeft: Int,
    private val paddingTop: Int,
    private val paddingRight: Int,
    private val paddingBottom: Int,
    var space: Float,
    val textSize: Float,
    val textColor: Int
) : IChartViewDraw {

    private lateinit var rect: RectF
    private val textPaint = Paint()
    private lateinit var items: List<ChartItem>
    private val clickList = mutableListOf<ClickChartItem>()

    override fun setupItems(items: List<ChartItem>) {
        this.items = items
    }

    override fun setupSpace(space: Float) {
        this.space = space
    }

    override fun onTouchEvent(context: Context, x: Float, y: Float) {
        clickList.firstOrNull { it.filterClickPredicate(x, it, y) }?.let {
            context.showToast((it.item.value).toString())
        }
    }

    override fun onDraw(canvas: Canvas?, paint: Paint, width: Int, height: Int) {
        val viewportWidth = (width - paddingLeft - paddingRight).toFloat()
        val viewportHeight = (height - paddingTop - paddingBottom).toFloat()
        val itemHeight = viewportHeight / items.size

        items.forEachIndexed { index, item ->
            calculateCoordinates(index, itemHeight, viewportWidth, item)
            canvas?.drawRect(rect, paint)
            canvas?.let {
                drawTextInsideRectangle(it, item.value.toInt().toString())
            }
        }
    }

    private fun calculateCoordinates(
        index: Int,
        itemHeight: Float,
        viewportWidth: Float,
        item: ChartItem
    ) {
        val l = paddingLeft.toFloat()
        val t = paddingTop + (index * itemHeight) + space / 2
        val r = paddingLeft + viewportWidth / 100F * item.value
        val b = paddingTop + (index + 1) * itemHeight - space / 2
        rect = RectF(l, t, r, b)
        clickList.add(ClickChartItem(index, item, Pair(l, r), Pair(t, b)))
    }

    private fun drawTextInsideRectangle(canvas: Canvas, str: String) {
        textPaint.textSize = textSize
        textPaint.color = textColor
        val xOffset = textPaint.measureText(str) * 0.5f
        val yOffset = textPaint.fontMetrics.ascent * -0.4f
        val textX = (rect.centerX()) - xOffset
        val textY = (rect.centerY()) + yOffset
        canvas.drawText(str, textX, textY, textPaint)
    }
}