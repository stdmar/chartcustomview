package com.example.customchartview1

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.os.Build
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi

class ChartView : View {

    private val VERTICAL_ORIENTATION = 10
    private val HORIZONTAL_ORIENTATION = 20

    var space: Float = 0F
        set(value) {
            field = value
            chartViewDraw?.setupSpace(value)
        }
    private var chartViewDraw: IChartViewDraw? = null
    private var orientation = VERTICAL_ORIENTATION
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var items = mutableListOf<ChartItem>()
    private var textColor: Int = 0
    private var textSize: Float = 0F

    fun setItems(tmpItems: List<ChartItem>) {
        items.clear()
        items.addAll(tmpItems)
        chartViewDraw?.setupItems(tmpItems)
    }

    constructor(context: Context?) : super(context) {
        println("Context constructor")
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        println("context: Context?, attrs: AttributeSet?")
        initChartView(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        println("context: Context?, attrs: AttributeSet?, defStyleAttr: Int")
        initChartView(attrs)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        println("context, attrs, defStyleAttr, defStyleRes")
        initChartView(attrs)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_UP ->{
                chartViewDraw?.onTouchEvent(context, event.x, event.y)
            }
        }
        return true
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (items.isEmpty()) return
        paint.color = Color.RED
        chartViewDraw?.onDraw(canvas, paint, width, height)

    }

    @SuppressLint("CustomViewStyleable")
    private fun initChartView(attrs: AttributeSet?) {
        attrs?.let {
            val ta = context.obtainStyledAttributes(it, R.styleable.ChartView)
            orientation = ta.getInt(R.styleable.ChartView_chart_orientation, VERTICAL_ORIENTATION)
            space = ta.getFloat(R.styleable.ChartView_item_space, 0f)
            textSize = ta.getFloat(R.styleable.ChartView_chart_text_size, 0F)
            textColor = ta.getColor(R.styleable.ChartView_chart_text_color, 0)
            initChartViewDraw()
            ta.recycle()
        }
    }

    private fun initChartViewDraw() {
        when (orientation) {
            VERTICAL_ORIENTATION -> {
                chartViewDraw = DrawVerticalChart(
                    paddingLeft = paddingLeft,
                    paddingTop = paddingTop,
                    paddingRight = paddingRight,
                    paddingBottom = paddingBottom,
                    space = space,
                    textSize = textSize,
                    textColor = textColor
                )
            }
            HORIZONTAL_ORIENTATION -> {
                chartViewDraw = DrawHorizontalChart(
                    paddingLeft = paddingLeft,
                    paddingTop = paddingTop,
                    paddingRight = paddingRight,
                    paddingBottom = paddingBottom,
                    space = space,
                    textSize = textSize,
                    textColor = textColor
                )
            }
        }
    }
}