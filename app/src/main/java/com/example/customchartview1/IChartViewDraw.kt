package com.example.customchartview1

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint

interface IChartViewDraw {
    fun setupSpace(space: Float)
    fun setupItems(items: List<ChartItem>)
    fun onTouchEvent(context: Context, x: Float, y: Float){}
    fun onDraw(canvas: Canvas?, paint: Paint, width: Int, height: Int)
}