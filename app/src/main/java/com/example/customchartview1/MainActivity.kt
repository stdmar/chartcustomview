package com.example.customchartview1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addItems()
        addHorizontalItems()
    }

    private fun addItems() {
        val tmpList = mutableListOf<ChartItem>()
        for (i in 1..7) {
            val item = ChartItem(Random.nextInt(20, 100).toFloat())
            tmpList.add(item)
        }
        chartView1?.setItems(tmpList)
    }

    private fun addHorizontalItems() {
        val tmpList = mutableListOf<ChartItem>()
        for (i in 1..5) {
            val item = ChartItem(Random.nextInt(20, 100).toFloat())
            tmpList.add(item)
        }
        chartViewHorizontal?.setItems(tmpList)
    }
}
